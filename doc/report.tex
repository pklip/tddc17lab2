\documentclass[fleqn,numbers=noenddot,headinclude,%twoside,%1headlines,%
				11pt,a4paper,footinclude,%
				cleardoublepage=empty,abstractoff %
                ]{scrartcl}

\usepackage[hyphens]{url}
\usepackage{float}

\def\tomas{Tom\'{a}\v{s} Peterka}
\def\tomasid{tompe625}
\def\philipp{Philipp Klippel}
\def\philippid{phikl641}

\def\laboratorynr{2}
\def\laboratoryname{Search}
\def\date{\today}


\input{preambl}

\lstset{style=java}

%\usepackage[subfigure]{tocloft}
%\renewcommand{\cftpartleader}{\cftdotfill{\cftdotsep}} % for parts
%\renewcommand{\cftchapleader}{\cftdotfill{\cftdotsep}} % for chapters
%\renewcommand{\cftsecleader}{\cftdotfill{\cftdotsep}} % for sections

\usepackage[sl,SF]{subfigure}
\usepackage{wrapfig}
\usepackage[hang,marginal]{footmisc}
\usepackage[justification=raggedright, margin=1cm,singlelinecheck=false,format=hang, labelfont={sl,small,rm},font={small,sf}, skip=10pt]{caption}
\renewcommand*\thesubfigure{\alph{subfigure}}

\makeatletter
\newcommand\wrapfill{\par
    \ifx\parshape\WF@fudgeparshape
    \nobreak
    \vskip-\baselineskip
    \vskip\c@WF@wrappedlines\baselineskip
    \allowbreak
    \WFclear
    \fi
}
\makeatother

\setlength{\intextsep}{6pt}

\usepackage{titlesec}
\titleformat{\section}{\large\bfseries}{\thesection}{1em}{}
\titleformat{\subsection}{\normalsize\bfseries}{\thesection}{1em}{}
\titleformat{\subsubsection}{\normalsize\bfseries}{\thesection}{1em}{}

\usepackage{amsmath}
\usepackage{graphicx}

\begin{document}

\makesavenoteenv[figure*]{figure}
\pagestyle{empty}
\onecolumn
%%Manuelle titelseite
\null  % Empty line
\nointerlineskip  % No skip for prev line
\vspace*{-1cm}
\begin{center}

\includegraphics[width=7cm,keepaspectratio=true]{./imgs/logo_hori.pdf}\\[0.3cm]
Department of Computer and Information Science

\vspace{1.2cm}

\Large TDDC17 -- Artificial Intelligence\\[0.5cm]
Laboratory \laboratorynr \\ \laboratoryname \normalsize\\


\vspace{0.7cm}

\begin{tabular}{cc}
\tomas & \philipp \\
\tomasid @student.liu.se & \philippid @student.liu.se\\
\end{tabular} \\[0.7cm]
\date
\end{center}
\vfill
\begin{abstract}
We implemented the common base of BFS\footnote{breadth-first search} and DFS\footnote{depth-first search} algorithms known as ''Graph search''. The only difference between BFS and DFS is the type of frontier used for storing \emph{opened} nodes. In the theoretical part we discuss informed search algorithms.
\end{abstract}
\vfill
\tableofcontents
\vfill
\clearpage

\pagestyle{scrheadings}
\setcounter{page}{1}


\section{Part 1 -- Vacuum Cleaner Agent}
\begin{lstlisting}
	// init: place start position to the frontier
	frontier.addNodeToFront(new SearchNode(startState));

	while(!frontier.isEmpty()) {
		node = frontier.removeFirst();
		// skip already explored node (if we queued one node twice)
		if(explored.contains(node)) {
			continue;
		}
		explored.add(node);
		
		if(p.isGoalState(node.getState())) {
			path = node.getPathFromRoot();
			break;
		}

		for(GridPos pos: p.getReachableStatesFrom(node.getState())) {
			// queue all neighbours
			SearchNode n = new SearchNode(pos);
			// policy function (simple in this case thus inline)
			if( !explored.contains(n) ) {
				n.setParent(node);
				storeNode(n);
			}
		}
	}
\end{lstlisting}
This is the common logic for all algorithms from \emph{x}FS family. The only difference is the type of the used frontier - queue, stack or priority queue. In this assignment we were supposed to program DFS and BFS.
\vfill
\clearpage

\section{Part 2 -- Theory}
\subsection*{Q1}
\emph{States} are positions in the grid (x and y) which are represented by the \verb|GridPos| class.\\
An \emph{Action} is withdrawing a new node from the frontier (aka moving to the next position). First we thought that an action means placing nodes into the frontier, but we decided that it is part of \emph{exploring}, dependent on the search technique.\\
The branching factor \emph{b} is defined as the maximal possible number of branches from one node which is \emph{b=4} in this case.

\subsection*{Q2}
Uniform Cost Search uses a priority queue instead of a normal queue like BFS. Since our case is, that all actions cost the same, uniform cost search and BFS will have exactly the same results.

\subsection*{Q3}
The only constraint on heuristic is $h(n) \le h^{*}(n)$
Therefore options a) and c) are right.

\subsection*{Q4}
As heuristic function $h(n)$ one can choose \emph{euclidean distance} or \emph{Manhattan distance}. Euclidean distance will always underestimate the real distance, because we are in a discrete world. Manhattan distance will return at maximum the same number as optimal heuristic, but since the world contains obstacles, it is more likely for the heuristic to underestimate. Both will never overestimate.\\
A cost function $g(n)$: can be any non-descending function. One function which makes sense is the depth of the current node in the search tree.

\subsection*{Q5}
\begin{figure}[H]
\subfigure[searching through maze]{\includegraphics[width=0.3\textwidth]{imgs/Astar.pdf}}\hfill
\subfigure[generated search tree]{\includegraphics[trim=3cm 14cm 2cm 1.4cm, clip=true,width=0.6\textwidth]{imgs/Astar-tree.pdf}}
\caption{A* Search}
\label{img:astar}
\end{figure}

\begin{figure}[H]
\subfigure[searching through maze]{\includegraphics[width=0.3\textwidth]{imgs/DFS.pdf}}\hfill
\subfigure[generated search tree]{\includegraphics[trim=3cm 8cm 5cm 1.4cm, clip=true,width=0.5\textwidth]{imgs/DFS-tree.pdf}}
\caption{Depth First Search}
\label{img:dfs}
\end{figure}

\begin{figure}[H]
\subfigure[searching through maze]{\includegraphics[width=0.3\textwidth]{imgs/BFS.pdf}}\hfill
\subfigure[generated search tree]{\includegraphics[trim=1cm 15.5cm 0.6cm 1.3cm, clip=true,width=0.6\textwidth]{imgs/BFS-tree.pdf}}
\caption{Breadth First Search}
\label{img:bfs}
\end{figure}

\subsection*{Q6}
The book discusses the algorithms below.

\subsubsection*{Depth-first search}
\emph{Complete: No}\\In an infinite state space the algorithm can choose the wrong branch and never find the solution.\\
\emph{Optimal: No}\\It doesn't search from the nearest to the furthest nodes. It just tries to find the goal with minimal resource demands.

\subsubsection*{Breadth-first search}
\emph{Complete: Yes}\\If there is a solution and the branching factor is finite then BFS is complete.
\emph{Optimal: Yes}\\If we don't consider cost as a decreasing function it is always optimal, because it goes from the shallowest to the furthest nodes. In our implementation it will be optimal only in case that all costs are the same (because of lines 7:9).

\subsubsection*{Iterative deepening search}
\emph{Complete: Yes}\\If the branching factor is finite.\\
\emph{Optimal: Yes}\\If all actions cost the same or at least the cost of actions is a non-decreasing function.

\subsubsection*{Bidirectional search}
\emph{Complete: Yes/No}\\Depending on the implementation (BFS or DFS) the completeness has the same condition like the used search algorithm.
\emph{Optimal: No}\\It requires additional verification that the path found is optimal. For example BFS has lower resource consumption in exchange for optimality.

\subsubsection*{Greedy best first search}
Searches only under usage of a heuristic function with DFS behaviour.\\
\emph{Complete: Yes/No}\\Tree search is incomplete because it can get stuck in a blind branch. Graph search is complete. Graph search means that it has a set of explored nodes so it doesn't make any mistake twice.\\
\emph{Optimal: No}\\It always takes the nearest node to the goal which may not be the nearest from global view.

\subsubsection*{A*}
\emph{Complete: Yes}\\Requires the existence of only a finite number of paths where the total cost equals the cost of the best path.
\emph{Optimal: Yes}\\If the heuristic function fulfills the constraint $h(n)<=h^{*}(n)$

\subsection*{Q7}
In the first lab it is possible to use any flooding algorithm to explore all nodes. DFS is the best choice for being really executed by a robot, because it does not do so many steps between nodes taken from the frontier. The goal should be to find something that does not exist (e.g. the ultimate question to the answer 42). Then the robot will explore all available nodes.

\end{document}
