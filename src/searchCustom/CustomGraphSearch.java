package searchCustom;

import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.HashSet;

import searchShared.NodeQueue;
import searchShared.Problem;
import searchShared.SearchObject;
import searchShared.SearchNode;

import world.GridPos;


public class CustomGraphSearch implements SearchObject {

	private HashSet<SearchNode> explored;
	private NodeQueue frontier;
	protected ArrayList<SearchNode> path;
	private boolean insertFront;

	/**
	 * The constructor tells graph search whether it should insert nodes to front or back of the frontier
	 */
    public CustomGraphSearch(boolean bInsertFront) {
		insertFront = bInsertFront;
    }

	/**
	 * Implements "graph search", which is the foundation of many search algorithms
	 */
	public ArrayList<SearchNode> search(Problem p) {
		/* Note: Returning an empty path signals that no path exists */

		// The frontier is a queue of expanded SearchNodes not processed yet
		frontier = new NodeQueue();
		/// The explored set is a set of nodes that have been processed
		explored = new HashSet<SearchNode>();
		// The start state is given
		GridPos startState = (GridPos) p.getInitialState();
		// Initialize the frontier with the start state
		frontier.addNodeToFront(new SearchNode(startState));

		// Path will be empty until we find the goal.
		path = new ArrayList<SearchNode>();

		SearchNode node;

		while(!frontier.isEmpty()) {
			node = frontier.removeFirst();
			if(explored.contains(node)){
				continue;
			}

			explored.add(node);
			if(p.isGoalState(node.getState())) {
				path = node.getPathFromRoot();
				break;
			}

			for(GridPos pos: p.getReachableStatesFrom(node.getState())) {
				//System.out.println("Expanding from node " + node.getState() + " to " + pos);
				SearchNode n = new SearchNode(pos);
				if( !explored.contains(n) ) {
					n.setParent(node);
					storeNode(n);
				}
			}


		}


		System.out.println("Found path:");
		printPath(path);
		return path;
	}

	private void storeNode(SearchNode node) {
		if(insertFront) {
			frontier.addNodeToFront(node);
		} else {
			frontier.addNodeToBack(node);
		}
	}

	private void printPath(ArrayList<SearchNode> path){
		for(int i = 0; i<path.size()-1; i++){
			System.out.print(path.get(i).getState() + "-->");
		}

		System.out.print(path.get(path.size()-1).getState() + "\n");
	}
	/**
	 * Suggested helper function for determining if a child node should be put on the frontier
	 */
	private boolean shouldExpandState(SearchNode childNode, SearchNode parentNode) {
		return false;
	}

	/*
	 * Functions below are just getters used externally by the program
	 */
	public ArrayList<SearchNode> getPath() {
		return path;
	}

	public ArrayList<SearchNode> getFrontierNodes() {
		return new ArrayList<SearchNode>(frontier.toList());
	}
	public ArrayList<SearchNode> getExploredNodes() {
		return new ArrayList<SearchNode>(explored);
	}
	public ArrayList<SearchNode> getAllExpandedNodes() {
		ArrayList<SearchNode> allNodes = new ArrayList<SearchNode>();
		allNodes.addAll(getFrontierNodes());
		allNodes.addAll(getExploredNodes());
		return allNodes;
	}

}
